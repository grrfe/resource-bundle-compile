# resource-bundle-compile

A gradle plugin that compiles JavaFX' "ResourceBundle" key-value mappings for internationalization to constants which can be used in code. 
