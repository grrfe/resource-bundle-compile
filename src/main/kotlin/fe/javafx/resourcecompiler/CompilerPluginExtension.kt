package fe.javafx.resourcecompiler

open class CompilerPluginExtension {
    var resourcePackage: String? = null
    var selfPackage: String? = null
    var resourceKeyFile: String? = null
    var resourceBundleDir: String? = null
}