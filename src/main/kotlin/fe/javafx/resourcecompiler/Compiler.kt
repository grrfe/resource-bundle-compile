package fe.javafx.resourcecompiler

import io.pebbletemplates.pebble.PebbleEngine
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import java.io.File

class Compiler : Plugin<Project> {
    companion object {
        const val COMPILE_SETTINGS = "javaFxResourceCompileSettings"
    }

    override fun apply(project: Project) {
        project.tasks.create("javaFxResourceCompile") { task ->
            val extension = project.extensions.create(COMPILE_SETTINGS, CompilerPluginExtension::class.java)

            task.doLast(CompilerTask(project.rootDir, extension))
        }.apply {
            group = "build"
        }
    }

    class CompilerTask(private val projectRootDir: File, private val extension: CompilerPluginExtension) :
        Action<Task> {

        companion object {
            private val ENGINE = PebbleEngine.Builder()
                .autoEscaping(false)
                .newLineTrimming(false)
                .build()
        }

        private fun hasRequiredConfig() = extension.resourcePackage != null
                && extension.resourceBundleDir != null
                && extension.resourceKeyFile != null


        private fun getKeys(files: List<File>): MutableMap<String, Int> {
            val keys = mutableMapOf<String, Int>()

            for (file in files) {
                file.bufferedReader().use { it.readLines() }.forEach { line ->
                    val eqIdx = line.indexOf("=")
                    if (eqIdx != -1) {
                        val key = line.substring(0, eqIdx)
                        if (!key.contains(" ")) {
                            keys.merge(key, 1) { ov, _ -> ov + 1 }
                        }
                    }
                }
            }

            return keys
        }

        private fun createConstantName(key: String): String {
            return buildString {
                var wasLastLower = false
                key.forEach { char ->
                    val isDot = char == '.'
                    if ((wasLastLower && char.isUpperCase()) || isDot) {
                        this.append("_")
                    }

                    wasLastLower = char.isLowerCase()
                    if (!isDot) {
                        this.append(char.toUpperCase())
                    }
                }
            }
        }

        override fun execute(p0: Task) {
            if (!hasRequiredConfig()) {
                return
            }

            val files = File(extension.resourceBundleDir!!).listFiles()?.filter { file ->
                file.isFile && file.extension == "properties"
            } ?: emptyList()

            val keys = getKeys(files)


            keys.forEach { (key, amount) ->
                if (amount != files.size) {
                    println("Warning, $key does not exist in all resource bundles")
                }
            }


            val file = File(projectRootDir, extension.resourceKeyFile!!).apply {
                this.parentFile.mkdirs()
            }

            file.bufferedWriter().use { writer ->
                ENGINE.getTemplate("template/keys.peb").evaluate(
                    writer, mapOf(
                        "fields" to keys.map { (key, _) ->
                            Field(createConstantName(key), key)
                        },
                        "package" to extension.resourcePackage!!,
                        "selfPackage" to extension.selfPackage
                    )
                )
            }
        }

        data class Field(val name: String, val key: String)
    }
}
