plugins {
    kotlin("jvm")
    `java-gradle-plugin`
    `maven-publish`
    id("net.nemerosa.versioning") version "3.0.0"
}

group = "fe.javafx.resource-bundle-compile"
version = versioning.info.tag ?: versioning.info.full

gradlePlugin {
    plugins {
        create("resource-bundle-compile") {
            id = project.group.toString()
            implementationClass = "fe.javafx.resourcecompiler.Compiler"
        }
    }
}


java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.pebbletemplates:pebble:3.2.2")
}

