pluginManagement {
    repositories {
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }

    plugins {
        kotlin("jvm") version "1.9.20"
        id("net.nemerosa.versioning") version "3.0.0"
    }
}



rootProject.name = "resource-bundle-compile"
